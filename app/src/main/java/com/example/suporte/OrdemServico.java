package com.example.suporte;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.text.DateFormat;
import java.util.Date;

public class OrdemServico extends AppCompatActivity {
    
    int id = 0;
    private SQLiteDatabase BancodeDados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ordem_servico);

    }

    String titulo, descricao;
    int autor;

    Date data = new Date();

    String DataAtual = DateFormat.getDateInstance(DateFormat.SHORT).format(data);


    public void AbrirChamado(){
        Log.i("LogUsuario", "Chamado aberto");
        BancodeDados = openOrCreateDatabase("meuBanco", MODE_PRIVATE,null);
        
        BancodeDados.execSQL("INSERT INTO ordem_servico(titulo, descricao, autor, data_pedido, fase, cliente) VALUES ('"+titulo+"', '"+descricao+"', '"+autor+"', '"+DataAtual+"',  1, "+id+")");
           
        Intent objetoIntent = new Intent(OrdemServico.this, ChamadosCliente.class);
        objetoIntent.putExtra("id",id);
        Log.i("LogUsuario", "Autor: " + id + "\nTitulo: " + titulo + "\nDescricao: " + descricao );
        startActivity(objetoIntent);
    }
    public void clicouBotaoAbrirChamado(View botao) {
        Log.i("LogUsuario", "Clicou no botao abrir novo chamado");

        EditText Titulo = findViewById(R.id.ETtitulo);
        EditText Descricao = findViewById(R.id.ETdescricao);
        Log.i("LogUsuario", "Chegou até aqui 1");

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            autor = extras.getInt("id");
            Log.i("LogUsuario", "Autor : "+autor);
        }
        Log.i("LogUsuario", "Chegou até aqui 2");
        
         id = autor;
        Log.i("LogUsuario", "Chegou até aqui 3");
        titulo = Titulo.getText().toString().trim();
        descricao = Descricao.getText().toString().trim();

        Log.i("LogUsuario", "Chegou até aqui 4");

        Log.i("LogUsuario", "Fim clicou botão");
        AbrirChamado();
    }






}
