package com.example.suporte;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ManterChamadoTecnico extends AppCompatActivity {


    int id = 0, idTec=0, fase=0, idTecnico=0;
    private SQLiteDatabase BancodeDados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manter_chamado_tecnico);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            id = extras.getInt("id");
            idTec = extras.getInt("idTecnico");
        }

        Log.i("LogUsuario", "O ID DO CHAMADO CLICADO É"+id);


        LinearLayout LayoutChamado = findViewById(R.id.DetalheChamadoTec);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(15, 15, 20, 10);

        final Button aceitar;
        final Button recusar;
        final Button encerrar;

        try {
            //Criando banco

            BancodeDados = openOrCreateDatabase("meuBanco", MODE_PRIVATE,null);



            //Criando cursor pra pegar todos os dados
            Cursor cursor = BancodeDados.rawQuery("SELECT * FROM ordem_servico WHERE id_ordemServico ="+id+" " , null);
            //pegando valores das colunas



            int indiceColunaOrdemServic = cursor.getColumnIndex("id_ordemServico");
            int indiceColunatituloOS = cursor.getColumnIndex("titulo");
            int indiceColunadescricaoOS = cursor.getColumnIndex("descricao");
            int indiceColunaautorOS = cursor.getColumnIndex("autor");
            int indiceColunaDataPedidoOS = cursor.getColumnIndex("data_pedido");
            int indiceColunacomprovanteOS = cursor.getColumnIndex("comprovante");
            int indiceColunafaseOS = cursor.getColumnIndex("fase");
            int indiceColunatecnicoOS = cursor.getColumnIndex("tecnico");


            //mover para o inicio

            cursor.moveToFirst();
            Log.i("LogBanco", "Listando as Ordem de Servico");
            if (cursor != null) {
                //exibindo valores

                Log.i("LogBanco", "Ordem de Serviço  - ID : " + cursor.getString(indiceColunaOrdemServic));
                Log.i("LogBanco", "Ordem de Serviço  - Titulo : " + cursor.getString(indiceColunatituloOS));
                Log.i("LogBanco", "Ordem de Serviço  - Descrição : " + cursor.getString(indiceColunadescricaoOS));
                Log.i("LogBanco", "Ordem de Serviço  - Autor : " + cursor.getString(indiceColunaautorOS));
                Log.i("LogBanco", "Ordem de Serviço  - Data : " + cursor.getString(indiceColunaDataPedidoOS));
                Log.i("LogBanco", "Ordem de Serviço  - Comprovante : " + cursor.getString(indiceColunacomprovanteOS));
                Log.i("LogBanco", "Ordem de Serviço  - Fase : " + cursor.getString(indiceColunafaseOS));
                Log.i("LogBanco", "Ordem de Serviço  - Tecnico : " + cursor.getString(indiceColunatecnicoOS));
                Log.i("LogBanco", "\n");


                fase = Integer.parseInt(cursor.getString(indiceColunafaseOS));

                final TextView TvChamados = new TextView(this);


                TvChamados.setTextColor(getResources().getColor(R.color.Background_Telas_Dark_TESTE));
                TvChamados.setTextSize(24);
                TvChamados.setId(Integer.parseInt(cursor.getString(indiceColunaOrdemServic)));
                TvChamados.setBackgroundResource(R.drawable.fundopersonalizadodois);
                //TvChamados.setBackgroundColor(getResources().getColor(R.color.Corda_Logo));
                TvChamados.setLayoutParams(lp);
                TvChamados.setText( " Codigo: " +cursor.getString(indiceColunaOrdemServic) + "\n Data : " + cursor.getString(indiceColunaDataPedidoOS)
                        + "\n\n Título: " + cursor.getString(indiceColunatituloOS) + "\n\n Descrição: " + cursor.getString(indiceColunadescricaoOS) + "\n\n autor: " + cursor.getString(indiceColunaautorOS));


                LayoutChamado.addView(TvChamados);

            }

            if(fase == 2) {
                Log.i("LogUsuario", "Fase 2");
                final EditText entradaValor;

                entradaValor = new EditText(this);
                entradaValor.setTextColor(getResources().getColor(R.color.Corda_Logo));
                entradaValor.setHint("Valor :");
                entradaValor.setHintTextColor(getResources().getColor(R.color.Corda_Logo));
                entradaValor.setInputType(InputType.TYPE_CLASS_NUMBER);

                encerrar = new Button(this);
                encerrar.setBackgroundResource(R.drawable.botaopersonalizado);
                encerrar.setLayoutParams(lp);
                encerrar.setText("ENCERRAR CHAMADO");
                encerrar.setTextSize(16);
                encerrar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (entradaValor.getText().length() == 0) {
                            Toast.makeText(ManterChamadoTecnico.this, "Necessita um valor", Toast.LENGTH_SHORT).show();
                            Log.i("LogUsuario", "Necessita um valor");
                        } else {
                            String Valor = entradaValor.getText().toString().trim();
                            Log.i("LogUsuario", "Clicou no botao Encerrar chamado");
                            BancodeDados.execSQL("UPDATE ordem_servico SET fase=3, valor='"+Valor+"' WHERE id_ordemServico=" + id);
                            Intent objetoIntent = new Intent(ManterChamadoTecnico.this, ChamadosTecnico.class);
                            objetoIntent.putExtra("id", idTec);
                            startActivity(objetoIntent);
                        }
                    }
                });

                final TextView TvStatus = new TextView(this);

                TvStatus.setTextColor(getResources().getColor(R.color.Corda_Logo));
                TvStatus.setTextSize(24);
                TvStatus.setLayoutParams(lp);
                TvStatus.setText( "Status : \n Em Andamento ");


                LayoutChamado.addView(TvStatus);
                LayoutChamado.addView(entradaValor);
                LayoutChamado.addView(encerrar);

            } else if(fase == 3){
                Log.i("LogUsuario", "Fase 3");

                final TextView TvStatus = new TextView(this);

                TvStatus.setTextColor(getResources().getColor(R.color.Corda_Logo));
                TvStatus.setTextSize(24);
                //TvChamados.setBackgroundColor(getResources().getColor(R.color.Corda_Logo));
                TvStatus.setLayoutParams(lp);
                TvStatus.setText( "Status : \n Aguardando confirmação de pagamento");


                LayoutChamado.addView(TvStatus);
            }
            else if(fase == 4 ){
                Log.i("LogUsuario", "Fase 4");


                final TextView TvStatus = new TextView(this);

                TvStatus.setTextColor(getResources().getColor(R.color.Corda_Logo));
                TvStatus.setTextSize(24);
                //TvChamados.setBackgroundColor(getResources().getColor(R.color.Corda_Logo));
                TvStatus.setLayoutParams(lp);
                TvStatus.setText( "Status : \n Encerrado ");


                LayoutChamado.addView(TvStatus);

            }






        } catch (Exception e){
            e.printStackTrace();
        }
    }

    }


