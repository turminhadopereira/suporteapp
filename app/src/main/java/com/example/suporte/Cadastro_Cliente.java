package com.example.suporte;

import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import java.util.InputMismatchException;


public class Cadastro_Cliente extends AppCompatActivity {

    private SQLiteDatabase BancodeDados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro__cliente);
    }
    String nome, data, cpf, email, endereco, cep, telefone, senha, ConfereSenha, sexo="";

    public void CadastroEfetuado() {

        BancodeDados = openOrCreateDatabase("meuBanco", MODE_PRIVATE,null);

        Log.i("LogUsuario", "Entrou no metodo");
        BancodeDados.execSQL("INSERT INTO pessoa(nome, sexo, cpf, email, endereco, cep, telefone, senha, tipo) VALUES ('"+nome+"', '"+sexo+"', '"+cpf+"', '"+email+"', '"+endereco+"', '"+cep+"', '"+telefone+"', '"+senha+"' , 1)");
        Log.i("LogUsuario", "Cadastro Efetuado");
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }

    public void Pegavalores(View view) {

        //feita para pegar os valores de todos os campos de texto e ver se todos estão preenchidos corretamente

        //declarando as variaveis e os Edit do xml pegando pelo ID

        Log.i("LogUsuario", "Chegou até aqui 01");
        EditText ETnome = findViewById(R.id.EntradaNome);
        EditText ETdata = findViewById(R.id.EntradaData);
        EditText ETcpf = findViewById(R.id.EntradaCpf);
        EditText ETemail = findViewById(R.id.EntradaEmail);
        EditText ETendereco = findViewById(R.id.EntradaEndereco);
        EditText ETcep = findViewById(R.id.EntradaCep);
        EditText ETtelefone = findViewById(R.id.EntradaTelefone);
        EditText ETsenha = findViewById(R.id.EntradaSenha);
        EditText ETConfereSenha = findViewById(R.id.EntradaConfirmarSenha);
        RadioButton masculino = findViewById(R.id.RadioMasculino);
        RadioButton feminino = findViewById(R.id.RadioFeminino);

        Log.i("LogUsuario", "Chegou até aqui 02");

        //pegando o texto de cada EditText
        nome = ETnome.getText().toString().trim();
        data = ETdata.getText().toString().trim();
        cpf = ETcpf.getText().toString().trim();
        email = ETemail.getText().toString().trim();
        endereco = ETendereco.getText().toString().trim();
        cep = ETcep.getText().toString().trim();
        telefone = ETtelefone.getText().toString().trim();
        senha = ETsenha.getText().toString().trim();
        ConfereSenha = ETConfereSenha.getText().toString().trim();

        if(masculino.isChecked()){
            sexo="M";
            Log.i("LogUsuario", "Masculino");
        }
        if(feminino.isChecked()){
            sexo="F";
            Log.i("LogUsuario", "Feminino");
        }
/*
        if (isCPF(cpf) == true) {
            System.out.printf("%s\n", imprimeCPF(cpf));
            Toast.makeText(Cadastro.this, "Cpf válido", Toast.LENGTH_SHORT).show();
        }
        else {
            System.out.printf("Erro, CPF invalido !!!\n");
            Toast.makeText(Cadastro.this, "Cpf inválido", Toast.LENGTH_SHORT).show();

        }*/


        Log.i("LogUsuario", "Chegou até aqui 03");

        //mostra valores inseridos
        //Toast.makeText(Cadastro.this, nome + "\n" + data + "\n" + sexo + "\n" + cpf + "\n" + email + "\n" + endereco + "\n" + cep + "\n" + telefone + "\n" + senha + "\n" + ConfereSenha, Toast.LENGTH_LONG).show();

        Log.i("LogUsuario", "Chegou até aqui 04");

        //Aqui vê se os campos estão preenchidos e retorna uma mensagem para o Usuario

        if (nome.length() == 0 | cpf.length() == 0 | email.length() == 0 | endereco.length() == 0 | telefone.length() < 10 | senha.length() < 6 | ConfereSenha.length() < 6) {
            //Toast.makeText(Cadastro.this, "Campo de preeenchimento obrigatório", Toast.LENGTH_SHORT).show();
            Log.i("LogUsuario", "Necessita um valor");
            if(nome.length() == 0){
                ETnome.setError("Campo nome é de preenchimento obrigatório");
                Toast.makeText(Cadastro_Cliente.this, "Campo nome é de preenchimento obrigatório", Toast.LENGTH_SHORT).show();
                Log.i("LogUsuario", "Campo nome é de preenchimento obrigatório");
            }
            if(cpf.length() != 11){
                ETcpf.setError("Cpf inválido");
                Toast.makeText(Cadastro_Cliente.this, "Cpf inválido", Toast.LENGTH_SHORT).show();
                Log.i("LogUsuario", "Cpf inválido");
            }
            if(email.length() == 0){
                ETemail.setError("Campo email é de preenchimento obrigatório");
                Toast.makeText(Cadastro_Cliente.this, "Campo email é de preenchimento obrigatório", Toast.LENGTH_SHORT).show();
                Log.i("LogUsuario", "Campo email é de preenchimento obrigatório");

            }
            if(endereco.length() == 0){
                ETendereco.setError("Campo endereço é de preenchimento obrigatório");
                Toast.makeText(Cadastro_Cliente.this, "Campo endereço é de preenchimento obrigatório", Toast.LENGTH_SHORT).show();
                Log.i("LogUsuario", "Campo endereço é de preenchimento obrigatório");
            }
            if(telefone.length() < 10){
                ETtelefone.setError("Telefone inválido digite o DDD e o número");
                Toast.makeText(Cadastro_Cliente.this, "Telefone inválido digite o DDD e o número", Toast.LENGTH_SHORT).show();
                Log.i("LogUsuario", "Telefone inválido digite o DDD e o número");
            }
            if(cep.length() != 8){
                ETcep.setError("CEP inválido");
                Toast.makeText(Cadastro_Cliente.this, "CEP inválido", Toast.LENGTH_SHORT).show();
                Log.i("LogUsuario", "CEP inválido");
            }
            if(senha.length() < 6 | ConfereSenha.length() < 6){
                ETsenha.setError("Campo senha e confere senha é de preenchimento obrigatório \n e devem ter pelomenos 6 dígitos");
                ETConfereSenha.setError("Campo senha e confere senha é de preenchimento obrigatório \n e devem ter pelomenos 6 dígitos");
                Toast.makeText(Cadastro_Cliente.this, "Campo senha e confere senha é de preenchimento obrigatório \n e devem ter pelomenos 6 dígitos", Toast.LENGTH_SHORT).show();
                Log.i("LogUsuario", "Campo senha e confere senha é de preenchimento obrigatório \n e devem ter pelomenos 6 dígitos");
            }
        } else {
            //Aqui compara a senha com a senha de confirmação pra vê se tá igual
            if (senha.equals(ConfereSenha)) {
                if(email.contains("@")){
                    if (isCPF(cpf) == true) {
                        System.out.printf("%s\n", imprimeCPF(cpf));
                        Log.i("LogUsuario", "CPF valido");
                        //Toast.makeText(CadastroEmpresa.this, "Cpf válido", Toast.LENGTH_SHORT).show();
                        CadastroEfetuado();
                        Log.i("LogUsuario", "Login Efetuado");
                    }
                    else {
                        ETcpf.setError("CPF inválido");
                        System.out.printf("Erro, CPF invalido !!!\n");
                        Toast.makeText(Cadastro_Cliente.this, "CPF inválido", Toast.LENGTH_SHORT).show();
                        Log.i("LogUsuario", "CPF invalido");
                    }
                }else{
                    ETemail.setError("Email inválido");
                    Toast.makeText(Cadastro_Cliente.this, "Email inválido", Toast.LENGTH_SHORT).show();
                    Log.i("LogUsuario", "Login Email invalido");
                }
            } else {
                ETsenha.setError("As senhas não conferem");
                ETConfereSenha.setError("As senhas não conferem");
                Toast.makeText(Cadastro_Cliente.this, "As senhas não conferem", Toast.LENGTH_LONG).show();
                Log.i("LogUsuario", "As senhas não conferem");
            }
        }

    }


    public static boolean isCPF(String CPF) {
        // considera-se erro CPF's formados por uma sequencia de numeros iguais
        if (CPF.equals("00000000000") ||
                CPF.equals("11111111111") ||
                CPF.equals("22222222222") || CPF.equals("33333333333") ||
                CPF.equals("44444444444") || CPF.equals("55555555555") ||
                CPF.equals("66666666666") || CPF.equals("77777777777") ||
                CPF.equals("88888888888") || CPF.equals("99999999999") ||
                (CPF.length() != 11))
            return(false);

        char dig10, dig11;
        int sm, i, r, num, peso;

        // "try" - protege o codigo para eventuais erros de conversao de tipo (int)
        try {
            // Calculo do 1o. Digito Verificador
            sm = 0;
            peso = 10;
            for (i=0; i<9; i++) {
                // converte o i-esimo caractere do CPF em um numero:
                // por exemplo, transforma o caractere '0' no inteiro 0
                // (48 eh a posicao de '0' na tabela ASCII)
                num = (int)(CPF.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }

            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11))
                dig10 = '0';
            else dig10 = (char)(r + 48); // converte no respectivo caractere numerico

            // Calculo do 2o. Digito Verificador
            sm = 0;
            peso = 11;
            for(i=0; i<10; i++) {
                num = (int)(CPF.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }

            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11))
                dig11 = '0';
            else dig11 = (char)(r + 48);

            // Verifica se os digitos calculados conferem com os digitos informados.
            if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10)))
                return(true);
            else return(false);
        } catch (InputMismatchException erro) {
            return(false);
        }
    }

    public static String imprimeCPF(String CPF) {
        return(CPF.substring(0, 3) + "." + CPF.substring(3, 6) + "." +
                CPF.substring(6, 9) + "-" + CPF.substring(9, 11));
    }
}


