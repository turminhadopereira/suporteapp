package com.example.suporte;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class ConfirmarPagamento extends AppCompatActivity {
    int id = 0, idCliente = 0;
    double avaliacao = 0, novaAv;
    private SQLiteDatabase BancodeDados;
    String valorPG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmar_pagamento);


        final SeekBar seekBar = findViewById(R.id.QtdAvaliacao);
        final TextView AvaliacaoTexto = findViewById(R.id.AvaliacaoTexto);
        final TextView ValorPg = findViewById(R.id.ValorPagamento);


        AvaliacaoTexto.setText("EXCELENTE !");
        AvaliacaoTexto.setTextColor(Color.parseColor("#00FF00"));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            id = extras.getInt("id");
            idCliente = extras.getInt("idCliente");
            valorPG = extras.getString("valor");
        }
        int valor;

        valor = seekBar.getProgress();

        ValorPg.setText("R$: "+valorPG+",00");

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                switch (progress){

                    case 0:
                        AvaliacaoTexto.setText("PÉSSIMO !");
                        AvaliacaoTexto.setTextColor(Color.parseColor("#ff0000"));
                        break;
                    case 1:
                        AvaliacaoTexto.setText("RUIM !");
                        AvaliacaoTexto.setTextColor(Color.parseColor("#ff0000"));

                        break;
                    case 2:
                        AvaliacaoTexto.setText("BOA !");
                        AvaliacaoTexto.setTextColor(Color.parseColor("#CBF22F"));

                        break;
                    case 3:
                        AvaliacaoTexto.setText("ÓTIMA !");
                        AvaliacaoTexto.setTextColor(Color.parseColor("#00FF00"));

                        break;
                    case 4:
                        AvaliacaoTexto.setText("EXCELENTE !");
                        AvaliacaoTexto.setTextColor(Color.parseColor("#00FF00"));

                        break;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        // Set up the user interaction to manually show or hide the system UI.


        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.

    }
    public void FuncaoAvaliacao(final View view){
        SeekBar seekBar = findViewById(R.id.QtdAvaliacao);
        final TextView AvaliacaoTexto = findViewById(R.id.AvaliacaoTexto);

        double valor;

        valor = seekBar.getProgress();

        Log.i("LogUsuario", "Avaliação é = "+ valor);

        try {
            //Criando banco

            BancodeDados = openOrCreateDatabase("meuBanco", MODE_PRIVATE,null);

            //BancodeDados.execSQL("DROP TABLE IF EXISTS pessoa");
            //BancodeDados.execSQL("DROP TABLE IF EXISTS ordem_servico");
            //Criando tabela



            //Criando cursor pra pegar todos os dados
            Cursor cursor = BancodeDados.rawQuery("SELECT * FROM pessoa WHERE tipo=2 ", null);
            //pegando valores das colunas



            int indiceColunaIdPessoa = cursor.getColumnIndex("id_pessoa");
            int indiceColunaNomePessoa = cursor.getColumnIndex("nome");
            int indiceColunaSexoPessoa = cursor.getColumnIndex("sexo");
            int indiceColunaCpfPessoa= cursor.getColumnIndex("cpf");
            int indiceColunaEmailPessoa = cursor.getColumnIndex("email");
            int indiceColunaSenhaPessoa = cursor.getColumnIndex("senha");
            int indiceColunaEndPessoa = cursor.getColumnIndex("endereco");
            int indiceColunaCepPessoa = cursor.getColumnIndex("cep");
            int indiceColunaTelefonePessoa = cursor.getColumnIndex("telefone");
            int indiceColunaTipoPessoa = cursor.getColumnIndex("tipo");
            int indiceColunaDisponibilidadePessoa = cursor.getColumnIndex("disponibilidade");
            int indiceColunaAvaliacaoPessoa = cursor.getColumnIndex("avaliacao");

            //mover para o inicio

            cursor.moveToFirst();
            Log.i("LogBanco", "Listando as Ordem de Servico");
            if (cursor != null) {
                //exibindo valores

               /* Log.i("LogBanco", "Pessoa  - ID : " + cursor.getString(indiceColunaIdPessoa));
                Log.i("LogBanco", "Pessoa  - Nome : " + cursor.getString(indiceColunaNomePessoa));
                Log.i("LogBanco", "Pessoa  - Sexo : " + cursor.getString(indiceColunaSexoPessoa));
                Log.i("LogBanco", "Pessoa  - Cpf : " + cursor.getString(indiceColunaCpfPessoa));
                Log.i("LogBanco", "Pessoa  - Email : " + cursor.getString(indiceColunaEmailPessoa));
                Log.i("LogBanco", "Pessoa  - Senha : " + cursor.getString(indiceColunaSenhaPessoa));
                Log.i("LogBanco", "Pessoa  - End : " + cursor.getString(indiceColunaEndPessoa));
                Log.i("LogBanco", "Pessoa  - Cep : " + cursor.getString(indiceColunaCepPessoa));
                Log.i("LogBanco", "Pessoa  - Telefone : " + cursor.getString(indiceColunaTelefonePessoa));
                Log.i("LogBanco", "Pessoa  - Tipo : " + cursor.getString(indiceColunaTipoPessoa));
                Log.i("LogBanco", "Pessoa  - Disponibilidade : " + cursor.getString(indiceColunaDisponibilidadePessoa));
                Log.i("LogBanco", "Pessoa  - Avaliacao : " + cursor.getString(indiceColunaAvaliacaoPessoa));*/

                avaliacao = Float.parseFloat(cursor.getString(indiceColunaAvaliacaoPessoa));

                valor = valor + avaliacao / 2;

                RadioButton sim = findViewById(R.id.RadioSim);
                RadioButton nao = findViewById(R.id.RadioNao);

                BancodeDados.execSQL("UPDATE pessoa SET avaliacao="+valor+" WHERE id_pessoa="+idCliente);
                if(sim.isChecked()){
                    BancodeDados.execSQL("UPDATE ordem_servico SET comprovante='PAGAMENTO EFETUADO', fase=4 WHERE id_ordemServico="+id);
                    Log.i("LogUsuario", "sim");

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mostrarListaChamados();
                        }
                    }, 2000);


                }else if(nao.isChecked()){
                    BancodeDados.execSQL("UPDATE ordem_servico SET comprovante='PAGAMENTO NEGADO', fase=4 WHERE id_ordemServico="+id);
                    Log.i("LogUsuario", "não");

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mostrarListaChamados();
                        }
                    }, 2000);

                } else{
                    Toast.makeText(ConfirmarPagamento.this, "Confirme o pagamento", Toast.LENGTH_SHORT).show();
                }

            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void mostrarListaChamados() {

        Intent intent = new Intent(
                ConfirmarPagamento.this, ChamadosCliente.class
        );
        intent.putExtra("id", idCliente);
        startActivity(intent);
        finish();
    }
    }



