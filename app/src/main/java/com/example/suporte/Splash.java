package com.example.suporte;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class Splash extends AppCompatActivity {

    private SQLiteDatabase BancodeDados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        try {
            //Criando banco

            BancodeDados = openOrCreateDatabase("meuBanco", MODE_PRIVATE,null);

          /*  BancodeDados.execSQL("DROP TABLE IF EXISTS pessoa");
            BancodeDados.execSQL("DROP TABLE IF EXISTS ordem_servico");*/
            //Criando tabela


            BancodeDados.execSQL("CREATE TABLE IF NOT EXISTS ordem_servico(id_ordemServico INTEGER PRIMARY KEY AUTOINCREMENT," +
                    " titulo VARCHAR NOT NULL," +
                    " descricao VARCHAR(255) NOT NULL," +
                    " autor VARCHAR NOT NULL," +
                    " data_pedido VARCHAR NOT NULL," +
                    " comprovante VARCHAR DEFAULT 0," +
                    " valor DOUBLE DEFAULT 0," +
                    " fase INTEGER NOT NULL," +
                    " tecnico INTEGER DEFAULT 0," +
                    " cliente INTEGER DEFAULT 0)");

            BancodeDados.execSQL("CREATE TABLE IF NOT EXISTS pessoa(id_pessoa INTEGER PRIMARY KEY AUTOINCREMENT," +
                    " nome VARCHAR NOT NULL," +
                    " sexo VARCHAR NOT NULL," +
                    " cpf VARCHAR NOT NULL," +
                    " email VARCHAR NOT NULL," +
                    " endereco VARCHAR NOT NULL," +
                    " cep VARCHAR NOT NULL," +
                    " telefone VARCHAR NOT NULL," +
                    " senha VARCHAR NOT NULL," +
                    " disponibilidade INTEGER DEFAULT 0," +
                    " tipo INTEGER NOT NULL," +
                    " avaliacao DOUBLE DEFAULT 0)");

            //inserindo dados

         /*  BancodeDados.execSQL("INSERT INTO ordem_servico(titulo, descricao, autor, data_pedido, fase, cliente) VALUES ('Formatação', 'Preciso que formate o meu computador instale o windows 10 e efetue o backup dos arquivos antigos', 2, '03/05/19', 1, 2)");
            BancodeDados.execSQL("INSERT INTO ordem_servico(titulo, descricao, autor, data_pedido, fase, cliente, tecnico) VALUES ('Limpeza no disco', 'Preciso que efetue uma limpeza no disco porque o computador está travando', 2, '03/05/19', 2, 2, 1)");
            BancodeDados.execSQL("INSERT INTO ordem_servico(titulo, descricao, autor, data_pedido, fase, cliente, tecnico, valor) VALUES ('Computador Reiniciando', 'Preciso que verifiquem o problema', 2, '03/05/19', 3, 2, 1, 200)");
            BancodeDados.execSQL("INSERT INTO ordem_servico(titulo, descricao, autor, data_pedido, fase, cliente, tecnico, valor) VALUES ('Limpeza de vírus', 'Preciso que efetuem uma limpeza de vírus no meu computador', 2, '03/05/19', 4, 2, 1, 150)");
            BancodeDados.execSQL("INSERT INTO ordem_servico(titulo, descricao, autor, data_pedido, fase, cliente) VALUES ('Computador não liga', 'Computador não liga', 2, '03/05/19', 1, 2)");


            BancodeDados.execSQL("INSERT INTO pessoa(nome, sexo, cpf, email, endereco, cep, telefone, senha, tipo) VALUES ('Filipe Torres', 'M', '07244537174', 'filipe@suporte.com', 'quadra 01 mr 05', '73751010', '93981999', 'minhasenha', 2)");

            BancodeDados.execSQL("INSERT INTO pessoa(nome, sexo, cpf, email, endereco, cep, telefone, senha, tipo) VALUES ('Filipe', 'M', '07244537174', 'filipe@gmail.com', 'quadra 01 mr 05', '73751010', '93981999', 'minhasenha' , 1)");

            BancodeDados.execSQL("INSERT INTO pessoa(nome, sexo, cpf, email, endereco, cep, telefone, senha, tipo) VALUES ('Administrador', 'M', '07244537174', 'filipe@admin.com', 'quadra 01 mr 05', '73751010', '93981999', 'minhasenha' , 3)");

            BancodeDados.execSQL("INSERT INTO pessoa(nome, sexo, cpf, email, endereco, cep, telefone, senha, tipo, disponibilidade) VALUES ('Douglas Vieira', 'M', '07244537174', 'douglas@suporte.com', 'quadra 05 mr 05', '73751010', '93981999', 'minhasenha', 2, 2)");
            BancodeDados.execSQL("INSERT INTO pessoa(nome, sexo, cpf, email, endereco, cep, telefone, senha, tipo) VALUES ('Luis Paulo', 'M', '07244537174', 'luis@suporte.com', 'quadra 06 mr 05', '73751010', '93981999', 'minhasenha', 2)");
*/


            //Criando cursor pra pegar todos os dados
            Cursor cursor = BancodeDados.rawQuery("SELECT * FROM pessoa, ordem_servico ", null);
            //pegando valores das colunas



            int indiceColunaOrdemServic = cursor.getColumnIndex("id_ordemServico");
            int indiceColunatituloOS = cursor.getColumnIndex("titulo");
            int indiceColunadescricaoOS = cursor.getColumnIndex("descricao");
            int indiceColunaautorOS = cursor.getColumnIndex("autor");
            int indiceColunaDataPedidoOS = cursor.getColumnIndex("data_pedido");
            int indiceColunacomprovanteOS = cursor.getColumnIndex("comprovante");
            int indiceColunavalorOS = cursor.getColumnIndex("valor");
            int indiceColunafaseOS = cursor.getColumnIndex("fase");
            int indiceColunatecnicoOS = cursor.getColumnIndex("tecnico");

            int indiceColunaIdPessoa = cursor.getColumnIndex("id_pessoa");
            int indiceColunaNomePessoa = cursor.getColumnIndex("nome");
            int indiceColunaSexoPessoa = cursor.getColumnIndex("sexo");
            int indiceColunaCpfPessoa= cursor.getColumnIndex("cpf");
            int indiceColunaEmailPessoa = cursor.getColumnIndex("email");
            int indiceColunaSenhaPessoa = cursor.getColumnIndex("senha");
            int indiceColunaEndPessoa = cursor.getColumnIndex("endereco");
            int indiceColunaCepPessoa = cursor.getColumnIndex("cep");
            int indiceColunaTelefonePessoa = cursor.getColumnIndex("telefone");
            int indiceColunaTipoPessoa = cursor.getColumnIndex("tipo");
            int indiceColunaDisponibilidadePessoa = cursor.getColumnIndex("disponibilidade");
            int indiceColunaAvaliacaoPessoa = cursor.getColumnIndex("avaliacao");


            //mover para o inicio

            cursor.moveToFirst();

            while (cursor != null) {
                //exibindo valores

                Log.i("LogBanco", "Ordem de Serviço  - ID : " + cursor.getString(indiceColunaOrdemServic));
                Log.i("LogBanco", "Ordem de Serviço  - Titulo : " + cursor.getString(indiceColunatituloOS));
                Log.i("LogBanco", "Ordem de Serviço  - Descrição : " + cursor.getString(indiceColunadescricaoOS));
                Log.i("LogBanco", "Ordem de Serviço  - Autor : " + cursor.getString(indiceColunaautorOS));
                Log.i("LogBanco", "Ordem de Serviço  - Data : " + cursor.getString(indiceColunaDataPedidoOS));
                Log.i("LogBanco", "Ordem de Serviço  - Comprovante : " + cursor.getString(indiceColunacomprovanteOS));
                Log.i("LogBanco", "Ordem de Serviço  - Valor : " + cursor.getString(indiceColunavalorOS));
                Log.i("LogBanco", "Ordem de Serviço  - Fase : " + cursor.getString(indiceColunafaseOS));
                Log.i("LogBanco", "Ordem de Serviço  - Tecnico : " + cursor.getString(indiceColunatecnicoOS));
                Log.i("LogBanco", "Chegou até aqui 6");

                Log.i("LogBanco", "Pessoa  - ID : " + cursor.getString(indiceColunaIdPessoa));
                Log.i("LogBanco", "Pessoa  - Nome : " + cursor.getString(indiceColunaNomePessoa));
                Log.i("LogBanco", "Pessoa  - Sexo : " + cursor.getString(indiceColunaSexoPessoa));
                Log.i("LogBanco", "Pessoa  - Cpf : " + cursor.getString(indiceColunaCpfPessoa));
                Log.i("LogBanco", "Pessoa  - Email : " + cursor.getString(indiceColunaEmailPessoa));
                Log.i("LogBanco", "Pessoa  - Senha : " + cursor.getString(indiceColunaSenhaPessoa));
                Log.i("LogBanco", "Pessoa  - End : " + cursor.getString(indiceColunaEndPessoa));
                Log.i("LogBanco", "Pessoa  - Cep : " + cursor.getString(indiceColunaCepPessoa));
                Log.i("LogBanco", "Pessoa  - Telefone : " + cursor.getString(indiceColunaTelefonePessoa));
                Log.i("LogBanco", "Pessoa  - Tipo : " + cursor.getString(indiceColunaTipoPessoa));
                Log.i("LogBanco", "Pessoa  - Disponibilidade : " + cursor.getString(indiceColunaDisponibilidadePessoa));
                Log.i("LogBanco", "Pessoa  - Avaliacao : " + cursor.getString(indiceColunaAvaliacaoPessoa));

                cursor.moveToNext();
            }

        } catch (Exception e){
            e.printStackTrace();
        }



        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mostrarMainActivity();
            }
        }, 2500);
    }




    private void mostrarMainActivity() {
        Intent intent = new Intent(
                Splash.this, Login.class
        );
        startActivity(intent);
        finish();
    }

}
