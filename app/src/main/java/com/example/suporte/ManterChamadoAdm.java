package com.example.suporte;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ManterChamadoAdm extends AppCompatActivity {

    private SQLiteDatabase BancodeDados;

    int id=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manter_chamado_adm);



        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            id = extras.getInt("id");
        }
        
        Log.i("LogUsuario", "O ID DO CHAMADO CLICADO É"+id);


        LinearLayout LayoutChamado = findViewById(R.id.LayoutChamado);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(15, 15, 20, 10);

        final Button aceitar;
        final Button recusar;

        try {
            //Criando banco

            BancodeDados = openOrCreateDatabase("meuBanco", MODE_PRIVATE,null);

            //BancodeDados.execSQL("DROP TABLE IF EXISTS pessoa");
            //BancodeDados.execSQL("DROP TABLE IF EXISTS ordem_servico");
            //Criando tabela



            //Criando cursor pra pegar todos os dados
            Cursor cursor = BancodeDados.rawQuery("SELECT * FROM ordem_servico WHERE id_ordemServico ="+id+" " , null);
            //pegando valores das colunas



            int indiceColunaOrdemServic = cursor.getColumnIndex("id_ordemServico");
            int indiceColunatituloOS = cursor.getColumnIndex("titulo");
            int indiceColunadescricaoOS = cursor.getColumnIndex("descricao");
            int indiceColunaautorOS = cursor.getColumnIndex("autor");
            int indiceColunaDataPedidoOS = cursor.getColumnIndex("data_pedido");
            int indiceColunacomprovanteOS = cursor.getColumnIndex("comprovante");
            int indiceColunafaseOS = cursor.getColumnIndex("fase");
            int indiceColunatecnicoOS = cursor.getColumnIndex("tecnico");


            //mover para o inicio

            cursor.moveToFirst();
            Log.i("LogBanco", "Listando as Ordem de Servico");
            if (cursor != null) {
                //exibindo valores

                Log.i("LogBanco", "Ordem de Serviço  - ID : " + cursor.getString(indiceColunaOrdemServic));
                Log.i("LogBanco", "Ordem de Serviço  - Titulo : " + cursor.getString(indiceColunatituloOS));
                Log.i("LogBanco", "Ordem de Serviço  - Descrição : " + cursor.getString(indiceColunadescricaoOS));
                Log.i("LogBanco", "Ordem de Serviço  - Autor : " + cursor.getString(indiceColunaautorOS));
                Log.i("LogBanco", "Ordem de Serviço  - Data : " + cursor.getString(indiceColunaDataPedidoOS));
                Log.i("LogBanco", "Ordem de Serviço  - Comprovante : " + cursor.getString(indiceColunacomprovanteOS));
                Log.i("LogBanco", "Ordem de Serviço  - Fase : " + cursor.getString(indiceColunafaseOS));
                Log.i("LogBanco", "Ordem de Serviço  - Tecnico : " + cursor.getString(indiceColunatecnicoOS));
                Log.i("LogBanco", "\n");

                final TextView TvChamados = new TextView(this);


                TvChamados.setTextColor(getResources().getColor(R.color.Background_Telas_Dark_TESTE));
                TvChamados.setTextSize(24);
                TvChamados.setId(Integer.parseInt(cursor.getString(indiceColunaOrdemServic)));
                TvChamados.setBackgroundResource(R.drawable.fundopersonalizadodois);
                //TvChamados.setBackgroundColor(getResources().getColor(R.color.Corda_Logo));
                TvChamados.setLayoutParams(lp);
                TvChamados.setText( " Codigo: " +cursor.getString(indiceColunaOrdemServic) + "\n Data : " + cursor.getString(indiceColunaDataPedidoOS)
                        + "\n\n Título: " + cursor.getString(indiceColunatituloOS) + "\n\n Descrição: " + cursor.getString(indiceColunadescricaoOS) + "\n\n Código Cliente: " + cursor.getString(indiceColunaautorOS));


                LayoutChamado.addView(TvChamados);

            }

            aceitar = new Button(this);
            aceitar.setBackgroundResource(R.drawable.botaopersonalizado);
            aceitar.setLayoutParams(lp);
            aceitar.setText("ACEITAR");
            aceitar.setTextSize(16);
            aceitar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.i("LogUsuario", "Clicou no botao aceitar chamado");
                    Intent objetoIntent = new Intent(ManterChamadoAdm.this, ListaTecnicos.class);
                    objetoIntent.putExtra("id", id);
                    objetoIntent.putExtra("caminho", 1);
                    startActivity(objetoIntent);

                }
            });

            recusar = new Button(this);
            recusar.setBackgroundResource(R.drawable.botaopersonalizado);
            recusar.setLayoutParams(lp);
            recusar.setText("RECUSAR");
            recusar.setTextSize(16);
            recusar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.i("LogUsuario", "Clicou no botao recusar chamado");

                    BancodeDados.execSQL("UPDATE ordem_servico SET fase=5, tecnico=0 WHERE id_ordemServico="+id);
                    Intent objetoIntent = new Intent(ManterChamadoAdm.this, ChamadosAdm.class);
                    startActivity(objetoIntent);
                }
            });

            LayoutChamado.addView(aceitar);
            LayoutChamado.addView(recusar);

        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
