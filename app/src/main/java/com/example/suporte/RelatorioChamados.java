package com.example.suporte;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

public class RelatorioChamados extends AppCompatActivity {

    private SQLiteDatabase BancodeDados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relatorio_chamados);


        LinearLayout LayoutChamados = findViewById(R.id.LayoutListaTec);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(15, 15, 15, 10);

        try {
            //Criando banco

            BancodeDados = openOrCreateDatabase("meuBanco", MODE_PRIVATE,null);


            //Criando cursor pra pegar todos os dados
            Cursor cursor = BancodeDados.rawQuery("SELECT * FROM ordem_servico ", null);
            //pegando valores das colunas



            int indiceColunaOrdemServic = cursor.getColumnIndex("id_ordemServico");
            int indiceColunatituloOS = cursor.getColumnIndex("titulo");
            int indiceColunadescricaoOS = cursor.getColumnIndex("descricao");
            int indiceColunaautorOS = cursor.getColumnIndex("autor");
            int indiceColunaDataPedidoOS = cursor.getColumnIndex("data_pedido");
            int indiceColunacomprovanteOS = cursor.getColumnIndex("comprovante");
            int indiceColunavalorOS = cursor.getColumnIndex("valor");
            int indiceColunafaseOS = cursor.getColumnIndex("fase");
            int indiceColunatecnicoOS = cursor.getColumnIndex("tecnico");


            //mover para o inicio

            cursor.moveToFirst();
            Log.i("LogBanco", "Listando as Ordem de Servico");
            while (cursor != null) {
                //exibindo valores


                final TextView TvChamados = new TextView(this);


                TvChamados.setTextColor(getResources().getColor(R.color.Background_Telas_Dark_TESTE));
                TvChamados.setTextSize(24);
                TvChamados.setId(Integer.parseInt(cursor.getString(indiceColunaOrdemServic)));
                TvChamados.setBackgroundResource(R.drawable.fundopersonalizadodois);
                //TvChamados.setBackgroundColor(getResources().getColor(R.color.Corda_Logo));
                TvChamados.setLayoutParams(lp);
                TvChamados.setText( " Codigo: " +cursor.getString(indiceColunaOrdemServic) + "\n Data : " + cursor.getString(indiceColunaDataPedidoOS)
                        + "\n Título: " + cursor.getString(indiceColunatituloOS)  +  "\n Descrição: " + cursor.getString(indiceColunadescricaoOS) + "\n Fase: " + cursor.getString(indiceColunafaseOS)+ "\n Valor: " + cursor.getString(indiceColunavalorOS) + "\n Código Cliente: " + cursor.getString(indiceColunaautorOS) + "\n Código Técnico: " + cursor.getString(indiceColunatecnicoOS));


                LayoutChamados.addView(TvChamados);


                cursor.moveToNext();
            }

        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
