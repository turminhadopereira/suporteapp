package com.example.suporte;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ChamadosTecnico extends AppCompatActivity {

    private SQLiteDatabase BancodeDados;
    int fase=0, id = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chamados_tecnico);

        LinearLayout LayoutChamados = findViewById(R.id.LayoutChamadosTec);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(15, 15, 15, 10);



        try {
            //Criando banco

            BancodeDados = openOrCreateDatabase("meuBanco", MODE_PRIVATE,null);


            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                id = extras.getInt("id");
                    Log.i("LogUsuario", "Id"+id);
            }


            Cursor cursor = BancodeDados.rawQuery("SELECT * FROM ordem_servico WHERE tecnico="+id+"", null);
            //pegando valores das colunas



            int indiceColunaOrdemServic = cursor.getColumnIndex("id_ordemServico");
            int indiceColunatituloOS = cursor.getColumnIndex("titulo");
            int indiceColunadescricaoOS = cursor.getColumnIndex("descricao");
            int indiceColunaautorOS = cursor.getColumnIndex("autor");
            int indiceColunaDataPedidoOS = cursor.getColumnIndex("data_pedido");
            int indiceColunacomprovanteOS = cursor.getColumnIndex("comprovante");
            int indiceColunafaseOS = cursor.getColumnIndex("fase");
            int indiceColunatecnicoOS = cursor.getColumnIndex("tecnico");


            //mover para o inicio

            cursor.moveToFirst();
            Log.i("LogBanco", "Listando as Ordem de Servico");
            while (cursor != null) {
                //exibindo valores

                Log.i("LogBanco", "Ordem de Serviço  - ID : " + cursor.getString(indiceColunaOrdemServic));
                Log.i("LogBanco", "Ordem de Serviço  - Titulo : " + cursor.getString(indiceColunatituloOS));
                Log.i("LogBanco", "Ordem de Serviço  - Descrição : " + cursor.getString(indiceColunadescricaoOS));
                Log.i("LogBanco", "Ordem de Serviço  - Autor : " + cursor.getString(indiceColunaautorOS));
                Log.i("LogBanco", "Ordem de Serviço  - Data : " + cursor.getString(indiceColunaDataPedidoOS));
                Log.i("LogBanco", "Ordem de Serviço  - Comprovante : " + cursor.getString(indiceColunacomprovanteOS));
                Log.i("LogBanco", "Ordem de Serviço  - Fase : " + cursor.getString(indiceColunafaseOS));
                Log.i("LogBanco", "Ordem de Serviço  - Tecnico : " + cursor.getString(indiceColunatecnicoOS));
                Log.i("LogBanco", "\n");

                final TextView TvChamados = new TextView(this);

                fase = Integer.parseInt(cursor.getString(indiceColunafaseOS));

                TvChamados.setTextColor(getResources().getColor(R.color.Background_Telas_Dark_TESTE));
                TvChamados.setTextSize(24);
                TvChamados.setId(Integer.parseInt(cursor.getString(indiceColunaOrdemServic)));
                if(fase == 4){
                    TvChamados.setBackgroundResource(R.drawable.fundopersonalizadovermelho);
                }
                else {
                    TvChamados.setBackgroundResource(R.drawable.fundopersonalizadodois);
                }
                //TvChamados.setBackgroundColor(getResources().getColor(R.color.Corda_Logo));
                TvChamados.setLayoutParams(lp);
                TvChamados.setText( " Codigo: " +cursor.getString(indiceColunaOrdemServic) + "\n Data : " + cursor.getString(indiceColunaDataPedidoOS)
                        + "\n Título: " + cursor.getString(indiceColunatituloOS));


                LayoutChamados.addView(TvChamados);

                TvChamados.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Log.i("LogUsuario", "Clicou Em algum Chamado");
                        int idChamado = TvChamados.getId();
                        Intent objetoIntent = new Intent(ChamadosTecnico.this, ManterChamadoTecnico.class);
                        objetoIntent.putExtra("id", idChamado);
                        objetoIntent.putExtra("idTecnico", id);
                        startActivity(objetoIntent);
                    }
                });




                cursor.moveToNext();
            }

        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
