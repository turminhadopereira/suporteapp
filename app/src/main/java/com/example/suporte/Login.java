package com.example.suporte;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class Login extends AppCompatActivity {

    private SQLiteDatabase BancodeDados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


    }

    String email, senha;

    public void LoginEfetuado(){

        try {
            //Criando banco

            BancodeDados = openOrCreateDatabase("meuBanco", MODE_PRIVATE,null);

            Cursor cursor = BancodeDados.rawQuery("SELECT id_pessoa, email, senha FROM pessoa WHERE email='"+email+"'", null);
            //pegando valores das colunas

            EditText Email = findViewById(R.id.emailCliente);
            EditText ETSenha = findViewById(R.id.senhaCliente);

            int indiceColunaIdPessoa = cursor.getColumnIndex("id_pessoa");
            int indiceColunaEmailPessoa = cursor.getColumnIndex("email");
            int indiceColunaSenhaPessoa = cursor.getColumnIndex("senha");


            Log.i("LogBanco", "\n\n");


            //mover para o inicio

            cursor.moveToFirst();
            Log.i("LogBanco", "Listando as Ordem de Servico");
            if (cursor != null) {
                //exibindo valores

                Log.i("LogBancoLogin", "Pessoa  - ID : " + cursor.getString(indiceColunaIdPessoa));
                Log.i("LogBancoLogin", "Pessoa  - Email : " + cursor.getString(indiceColunaEmailPessoa));
                Log.i("LogBancoLogin", "Pessoa  - Senha : " + cursor.getString(indiceColunaSenhaPessoa));
                Log.i("LogBanco", "\n");

                String Id = cursor.getString(indiceColunaIdPessoa);
                String Senha = cursor.getString(indiceColunaSenhaPessoa);

                int id = Integer.parseInt(Id);

                if(senha.equals(Senha)){
                    Log.i("LogUsuario", "Senha Correta");

                    if(email.contains("@admin.com")){
                        //email de administrador
                        Log.i("LogUsuario", "Login como administrador");
                        Intent objetoIntent = new Intent(Login.this, ChamadosAdm.class);
                        startActivity(objetoIntent);

                    }else if (email.contains("@suporte.com")){
                        //email dos tecnicos
                        Log.i("LogUsuario", "Login como técnico");
                        Intent objetoIntent = new Intent(Login.this, ChamadosTecnico.class);
                        objetoIntent.putExtra("id", id);
                        startActivity(objetoIntent);
                    }else{
                        //email geral ou seja cliente
                        Log.i("LogUsuario", "Login como cliente");
                        Intent objetoIntent = new Intent(Login.this, ChamadosCliente.class);
                        //envia valor para a activity 7
                        objetoIntent.putExtra("id", id);
                        startActivity(objetoIntent);
                    }
                }else {
                    Log.i("LogUsuario", "Senha Incorreta");
                    ETSenha.setError("Senha incorreta");
                }

            }
                SemCadastro();

        } catch (Exception e){
            e.printStackTrace();
        }


    }
    public void clicouBotaoLogin(View botao) {
        Log.i("LogUsuario", "Clicou no botao login cliente");


        EditText Email = findViewById(R.id.emailCliente);
        EditText ETSenha = findViewById(R.id.senhaCliente);



        email = Email.getText().toString().trim();
        senha = ETSenha.getText().toString().trim();

        //valida o email
        if(email.contains("@") && email.contains(".")){
            LoginEfetuado();
        } else {
            Log.i("LogUsuario", "Não tem @ ou .");
            Email.setError("Email inválido");
        }

    }

    public void SemCadastro() {
        Log.i("LogUsuario", "Entrou no metodo sem cadastro");


        EditText Email = findViewById(R.id.emailCliente);
        EditText ETSenha = findViewById(R.id.senhaCliente);

        Email.setError("Não possui cadastro");

    }

    public void clicouBotaoSemCadastro(View botao) {
        Log.i("LogUsuario", "Clicou no botao tecnico");
        Intent objetoIntent = new Intent(Login.this, Cadastro_Cliente.class);
        startActivity(objetoIntent);
    }

    public String getEmail() {

        return this.email;
    }
}
