package com.example.suporte;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class ListaTecnicos extends AppCompatActivity {


    private SQLiteDatabase BancodeDados;
    int idChamado=0, disponibilidade=0, caminho=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_lista_tecnicos);

        FloatingActionButton fab = findViewById(R.id.fabCadastrar);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("LogUsuario", "Clicou botão cadastrar tecnicos");
                Intent objetoIntent = new Intent(ListaTecnicos.this, Cadastro_Tecnico.class);
                startActivity(objetoIntent);
            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            idChamado = extras.getInt("id");
            caminho = extras.getInt("caminho");
        }

        LinearLayout LayoutTecnicos = findViewById(R.id.LayoutTecnicos);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(15, 15, 15, 10);

        try {
            //Criando banco

            BancodeDados = openOrCreateDatabase("meuBanco", MODE_PRIVATE,null);

            //BancodeDados.execSQL("DROP TABLE IF EXISTS pessoa");
            //BancodeDados.execSQL("DROP TABLE IF EXISTS ordem_servico");
            //Criando tabela



            //Criando cursor pra pegar todos os dados
            Cursor cursor = BancodeDados.rawQuery("SELECT * FROM pessoa WHERE tipo=2 ", null);
            //pegando valores das colunas



            int indiceColunaIdPessoa = cursor.getColumnIndex("id_pessoa");
            int indiceColunaNomePessoa = cursor.getColumnIndex("nome");
            int indiceColunaSexoPessoa = cursor.getColumnIndex("sexo");
            int indiceColunaCpfPessoa= cursor.getColumnIndex("cpf");
            int indiceColunaEmailPessoa = cursor.getColumnIndex("email");
            int indiceColunaSenhaPessoa = cursor.getColumnIndex("senha");
            int indiceColunaEndPessoa = cursor.getColumnIndex("endereco");
            int indiceColunaCepPessoa = cursor.getColumnIndex("cep");
            int indiceColunaTelefonePessoa = cursor.getColumnIndex("telefone");
            int indiceColunaTipoPessoa = cursor.getColumnIndex("tipo");
            int indiceColunaDisponibilidadePessoa = cursor.getColumnIndex("disponibilidade");
            int indiceColunaAvaliacaoPessoa = cursor.getColumnIndex("avaliacao");

            //mover para o inicio

            cursor.moveToFirst();
            Log.i("LogBanco", "Listando as Ordem de Servico");
            while (cursor != null) {
                //exibindo valores


                final TextView TvTecnicos = new TextView(this);

                disponibilidade = Integer.parseInt(cursor.getString(indiceColunaDisponibilidadePessoa));

                TvTecnicos.setTextColor(getResources().getColor(R.color.Background_Telas_Dark_TESTE));
                TvTecnicos.setTextSize(24);
                TvTecnicos.setId(Integer.parseInt(cursor.getString(indiceColunaIdPessoa)));

                if(caminho == 1) {

                    if (disponibilidade == 2) {
                        TvTecnicos.setBackgroundResource(R.drawable.fundopersonalizadovermelho);
                        TvTecnicos.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Log.i("LogUsuario", "Clicou no botao tecnico indisponivel");

                                Toast.makeText(ListaTecnicos.this, "Técnico indisponível no momento", Toast.LENGTH_SHORT).show();

                            }
                        });
                    } else {
                        TvTecnicos.setBackgroundResource(R.drawable.fundopersonalizadodois);

                        TvTecnicos.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Log.i("LogUsuario", "Clicou no botao tecnico");
                                int id = TvTecnicos.getId();
                                Intent objetoIntent = new Intent(ListaTecnicos.this, ChamadosAdm.class);
                                BancodeDados.execSQL("UPDATE ordem_servico SET fase=2, tecnico=" + id + " WHERE id_ordemServico=" + idChamado);
                                startActivity(objetoIntent);
                            }
                        });
                    }
                    //TvTecnicos.setBackgroundColor(getResources().getColor(R.color.Corda_Logo));
                    TvTecnicos.setLayoutParams(lp);
                    TvTecnicos.setText(" Codigo: " + cursor.getString(indiceColunaIdPessoa) + "\n Nome : " + cursor.getString(indiceColunaNomePessoa)
                            + "\n Avaliacao: " + cursor.getString(indiceColunaAvaliacaoPessoa));


                    LayoutTecnicos.addView(TvTecnicos);
                }else {
                    TvTecnicos.setBackgroundResource(R.drawable.fundopersonalizadodois);
                    TvTecnicos.setLayoutParams(lp);
                    TvTecnicos.setText( " Codigo: " +cursor.getString(indiceColunaIdPessoa) + "\n Nome : " + cursor.getString(indiceColunaNomePessoa)
                            + "\nCPF : " + cursor.getString(indiceColunaCpfPessoa) + " \nSexo : " + cursor.getString(indiceColunaSexoPessoa)  + "\nEmail : " + cursor.getString(indiceColunaEmailPessoa)  + "\nEndereço : " + cursor.getString(indiceColunaEndPessoa) + "\n Avaliacao: " + cursor.getString(indiceColunaAvaliacaoPessoa) + "\nDisponibilidade : " + cursor.getString(indiceColunaDisponibilidadePessoa));
                    LayoutTecnicos.addView(TvTecnicos);
                }




                cursor.moveToNext();
            }

        } catch (Exception e){
            e.printStackTrace();
        }

    }
}